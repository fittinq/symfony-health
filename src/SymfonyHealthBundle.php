<?php declare(strict_types=1);

namespace Fittinq\Symfony\Health;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class SymfonyHealthBundle extends Bundle
{
}
