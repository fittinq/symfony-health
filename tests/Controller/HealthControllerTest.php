<?php declare(strict_types=1);

namespace Test\Fittinq\Symfony\Health\Controller;

use Fittinq\Symfony\Health\Controller\HealthController;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Response;

class HealthControllerTest extends TestCase
{
    private HealthController $healthController;

    protected function setUp(): void
    {
        parent::setUp();

        $this->healthController = new HealthController();
    }

    public function test_requestEndpoint_returnHTTPOKResponse(): void
    {
        $response = $this->healthController->getHealth();
        $this->assertEquals(Response::HTTP_OK, $response->getStatusCode());
    }
}
